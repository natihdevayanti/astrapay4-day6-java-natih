package Tugas5;


import java.util.ArrayList;
import java.util.Scanner;

interface Worker{
    abstract void tambahAbsensi();
}

class Staff implements Worker {
    int gajiPokok;
    int IDKaryawan;
    String nama;
    int absensi = 20;

    public Staff(int IDKaryawan, String nama, int gajiPokok) {
        this.gajiPokok = gajiPokok;
        this.IDKaryawan = IDKaryawan;
        this.nama = nama;
    }

    @Override
    public void tambahAbsensi(){
        this.absensi = this.absensi + 1;
        System.out.println("Absensi berhasil ditambahkan");
    }

    public int getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(int gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public int getIDKaryawan() {
        return IDKaryawan;
    }

    public void setIDKaryawan(int IDKaryawan) {
        this.IDKaryawan = IDKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAbsensi() {
        return absensi;
    }

    public void setAbsensi(int absensi) {
        this.absensi = absensi;
    }
}

class Tugas5 {
    public static void main(String[] args) throws Exception{
        int pilihanMenu;
        int id;
        String nama;
        int gajiPokok;
        ArrayList<Staff> arrayStaff = new ArrayList<Staff>();
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("MENU");
            System.out.println("1. Buat Staff");
            System.out.println("2. Absensi Staff");
            System.out.println("3. Laporan Absensi");
            System.out.println("4. EXIT");
            System.out.print("Input nomor : ");
            pilihanMenu = input.nextInt();
            if (pilihanMenu == 1) {
                System.out.print("Masukkan id : ");
                id = input.nextInt();
                System.out.print("Masukkan nama : ");
                nama = input.next();
                System.out.print("Masukkan gaji pokok : ");
                gajiPokok = input.nextInt();
                Staff stf = new Staff(id, nama, gajiPokok);
                arrayStaff.add(stf);
            } else if (pilihanMenu == 2) {
                System.out.print("Masukkan ID : ");
                int inputID = input.nextInt();
                for (Staff s : arrayStaff) {
                    if (s.getIDKaryawan() == inputID) {
                        s.tambahAbsensi();
                        break;
                    }
                }
            }else if (pilihanMenu == 3) {
                String leftAlignFormat = "| %-4d | %-15s | %-15s | %-15s |%n";
                System.out.format("+------+-----------------+-----------------+-----------------+%n");
                System.out.format("| ID   | Nama            | Gaji Pokok      | Absensi/Hari    |%n");
                System.out.format("+------+-----------------+-----------------+-----------------+%n");
                for (Staff s : arrayStaff) {
                    System.out.format(leftAlignFormat, s.getIDKaryawan(), s.getNama(), s.getGajiPokok(), s.getAbsensi());
                }
                System.out.format("+------+-----------------+-----------------+-----------------+%n");
            } else if (pilihanMenu == 4) {
                break;
            } else System.out.println("Pilihan menu tidak ada");
        }while (true);
    }
}
