package Tugas3;


class Parent {
    String name = "Mr. USD";
    int money = 2000000;

//    public Parent(String name, int money) {
//        this.name = name;
//        this.money = money;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void home(){
        System.out.println("Parent's Home");
    }

    public void car(){
        System.out.println("Parent's Car");
    }
}

class Child extends Parent{
    String name = "Tom";
    int money = 200;

//    public Child(String name, int money) {
//        super(name, money);
//        this.name = name;
//        this.money = money;
//    }

//    public Child(String name, int money, String name1, int money1) {
//        super(name, money);
//        this.name = name1;
//        this.money = money1;
//    }

    @Override
    public void car(){
        System.out.println("Child's Car");
    }

    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public int getMoney() {
        return money;
    }
    @Override
    public void setMoney(int money) {
        this.money = money;
    }
    public void parentInfo(){
        System.out.println("Parent Name : " + super.getName());
        System.out.println("Parent Money : $ " + super.getMoney());
    }

    public void childInfo(){
        System.out.println("Name : " + getName());
        System.out.println("Money : $ " + getMoney());
    }
}

public class Tugas3 {
    public static void main(String[] args) {
        Child chld = new Child();
        chld.home();
        chld.car();
        System.out.println();
        chld.parentInfo();
        System.out.println();
        chld.childInfo();

    }
}
