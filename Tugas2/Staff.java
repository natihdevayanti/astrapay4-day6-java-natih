package Tugas2;

import java.util.ArrayList;
import java.util.Scanner;

class Worker {
    int IDKaryawan;
    String nama;

    public Worker(int IDKaryawan, String nama) {
        this.IDKaryawan = IDKaryawan;
        this.nama = nama;
    }

    public int getIDKaryawan() {
        return IDKaryawan;
    }

    public void setIDKaryawan(int IDKaryawan) {
        this.IDKaryawan = IDKaryawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}

class Staff extends Worker {
    String jabatan;

    public Staff(int IDKaryawan, String nama, String jabatan) {
        super(IDKaryawan, nama);
        this.jabatan = jabatan;
    }
    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
}

class Inheritance {
    public static void main(String[] args) throws Exception{
        int pilihanMenu;
        int id;
        String nama;
        String jabatan;
        ArrayList<Staff> arrayStaff = new ArrayList<Staff>();
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("MENU");
            System.out.println("1. Buat Staff");
            System.out.println("2. Tampilkan Laporan Staff");
            System.out.println("3. EXIT");
            System.out.print("Input nomor : ");
            pilihanMenu = input.nextInt();
            if (pilihanMenu == 1) {
                System.out.print("Masukkan id : ");
                id = input.nextInt();
                System.out.print("Masukkan nama : ");
                nama = input.next();
                System.out.print("Masukkan jabatan : ");
                jabatan = input.next();
                Staff stf = new Staff(id, nama, jabatan);
                arrayStaff.add(stf);
            } else if (pilihanMenu == 2) {
                String leftAlignFormat = "| %-4d | %-15s | %-15s |%n";
                System.out.format("+------+-----------------+-----------------+%n");
                System.out.format("| ID   | Nama            | Jabatan         |%n");
                System.out.format("+------+-----------------+-----------------+%n");
                for (Staff s : arrayStaff) {
                    System.out.format(leftAlignFormat, s.getIDKaryawan(), s.getNama(), s.getJabatan());
                }
                System.out.format("+------+-----------------+-----------------+%n");
            } else if (pilihanMenu == 3) {
                break;
            } else System.out.println("Pilihan menu tidak ada");
        }while (true);
    }
}
