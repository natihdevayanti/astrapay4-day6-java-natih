package Tugas1;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Tugas1 {
    public static void main(String[] args) throws Exception{
        Scanner input = new Scanner(System.in);
        ArrayList<Mahasiswa> arrayMahasiswa = new ArrayList<Mahasiswa>();
        int pilihanMenu;
        int id;
        int nilai;
        String nama;

        do {
            System.out.println("MENU");
            System.out.println("1. Input Data Mahasiswa");
            System.out.println("2. Laporan");
            System.out.println("3. EXIT");
            System.out.print("Input nomor : ");
            pilihanMenu = input.nextInt();
            if (pilihanMenu == 1) {
                ///Users/ada-nb184/IdeaProjects/JavaDay06Task/src/NamaFile/
                System.out.print("Masukkan nama directory : ");
                String inputDirectory = input.next();
                //Mahasiswa.txt
                System.out.print("Masukkan nama file : ");
                String inputFilename = input.next();

                FileReader fr = new FileReader(inputDirectory + inputFilename);
                int i = 0;
                //Read per character
                String strAccu = "";
                //Read per character
                while((i=fr.read()) != -1) {
                    strAccu = strAccu + (char) i;
                }
                String strAccuParsed [] = strAccu.split("\n");

                for(int j=0;j<strAccuParsed.length;j++){
                    System.out.println(strAccuParsed[j]);
                    if(j > 0)
                    {
                        String strAccuParsed2 [] = strAccuParsed[j].trim().split(",");

                        id = Integer.parseInt(strAccuParsed2[0]);
                        nama = strAccuParsed2[1];
                        nilai = Integer.parseInt(strAccuParsed2[2]);

                        Mahasiswa mhs = new Mahasiswa(id, nama, nilai);
                        arrayMahasiswa.add(mhs);
                    }
                }

                fr.close();
//                System.out.print("Masukkan id : ");
//                id = input.nextInt();
//                System.out.print("Masukkan nama : ");
//                nama = input.next();
//                System.out.print("Masukkan nilai : ");
//                nilai = input.nextInt();
//
//                System.out.println(id + "," + nama + "," + nilai);
            } else if (pilihanMenu == 2) {
                System.out.print("Masukkan nama directory : ");
                String inputDir = input.next();
                System.out.print("Masukkan nama file : ");
                String inputName = input.next();
                try{
                    FileWriter fw = new FileWriter(inputDir + inputName);
                    for (Mahasiswa s : arrayMahasiswa) {
                        fw.write("ID : " + s.getId());
                        fw.write("\n");
                        fw.write("Nama : " + s.getNama());
                        fw.write("\n");
                        fw.write("Nilai : " + s.getNilai());
                        fw.write("\n");
                    }
                    fw.close();
                }catch(Exception e){System.out.println(e);}
                System.out.println("Success...");

//                FileReader fr=new FileReader(inputDir + inputName);
//                int i = 0;
//                //Read per character
//                while((i=fr.read()) != -1)
//                    System.out.print((char)i);
//                System.out.println();
//                fr.close();

            } else if (pilihanMenu == 3) {
                break;
            } else System.out.println("Pilihan menu tidak ada");
        }while (true);
    }
}
